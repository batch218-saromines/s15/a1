// console.log("Hello World");

/*
	1. Create variables to store to the following user details:

	-first name - String
	-last name - String
	-age - Number
	-hobbies - Array
	-work address - Object

		-The hobbies array should contain at least 3 hobbies as Strings.
		-The work address object should contain the following key-value pairs:

			houseNumber: <value>
			street: <value>
			city: <value>
			state: <value>

	Log the values of each variable to follow/mimic the output.

	Note:
		-Name your own variables but follow the conventions and best practice in naming variables.
		-You may add your own values but keep the variable names and values Safe For Work.
*/

	//Add your variables and console log for objective 1 here:




/*			
	2. Debugging Practice - Identify and implement the best practices of creating and using variables 
	   by avoiding errors and debugging the following codes:

			-Log the values of each variable to follow/mimic the output.
*/	




	// 1.



	let firstName="Steve"
	console.log("First Name: "+ firstName);

	let lastName="Jobs"
	console.log("Last Name: "+ lastName);


	let age="40"
	console.log("Age: "+ age);



	console.log("Hobbies:")
	let hobbies = ["sleeping","playing","reading"];
	console.log(hobbies);

	console.log("Work Address:")
	let workAddress={
	houseNumber: "312",
	street: "avenue",
	city:"Washington",
	state:"MaryLand",
				}
	console.log(workAddress)


	// 2.


	let fullName = "Steve Rogers";
	console.log("My full name is: " + fullName);

	let currentAge = "40";
	console.log("My current age is: " + currentAge);
	

	console.log("My friends are:")
	let friends = ["Tony","Bruce","Thor","Natasha","Clint","Nick"];
	console.log(friends);


	console.log("My Full Profile: ")
	let profile={
	username: "captain_america",
	age:35,
	isActive:false,
				}
	console.log(profile)


	let bestfName = "Bucky Barnes";
	console.log("My best friend is: " + bestfName);


	const lastLocation = "Arctic Ocean";
	// lastLocation = "Atlantic Ocean";
	console.log("I was found frozen in: " + lastLocation);


